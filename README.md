# Web vulnerabilities
This repository contains a web application which serves as an example of current web vulnerabilities and their effects. 
Students and enthusiasts alike can use this setup in order to learn 
about these vulnerabilities, how they work and what can be done to prevent them.

# Running the application
* Install Docker: <https://docs.docker.com/install/>
* Install Docker Compose: <https://docs.docker.com/compose/install/>
* Navigate to the root folder
* Build the Docker images: `docker-compose build`
* Start the application: `docker-compose up`
* Navigate to <http://localhost:80> for the instruction page

NOTE: 
You need to run this on localhost, otherwise some functionalities will not work

# Technology stack
* Front-ends:
  * NGINX
  * Javascript, HTML5, CSS3
* Back-end:
  * Node.js
  * Express
  * Kotlin (Spring)
* Database:
  * PostgreSQL
  
## Changelog
# V1
* SQL Injection
* XSS
* CSRF
* Insecure Direct Object References
# V2 (Robin & Rowan)
* OAuth2 Authorization Code Vulnerabilities
    * Added OAuth2 Server in Kotlin + Spring
    * Implemented OAuth2 Authorization Code flow in JS
* DOM-based Cookie Manipulation
    * Added dynamic rendering in Express to render vulnerable html page
    * 
