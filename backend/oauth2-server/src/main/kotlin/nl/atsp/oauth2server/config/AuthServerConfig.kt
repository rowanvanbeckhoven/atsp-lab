package nl.atsp.oauth2server.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.factory.PasswordEncoderFactories.createDelegatingPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer

@Configuration
class AuthorizationServerConfig() :
    AuthorizationServerConfigurerAdapter()
{

    override fun configure(security: AuthorizationServerSecurityConfigurer?)
    {
        security!!
            .tokenKeyAccess("permitAll()")
            .checkTokenAccess("isAuthenticated()")
    }

    @Bean
    internal fun passwordEncoder(): PasswordEncoder {
        return createDelegatingPasswordEncoder()
    }

    override fun configure(clients: ClientDetailsServiceConfigurer?)
    {
        clients!!
            .inMemory()
            .withClient("atsp-frontend").secret(passwordEncoder().encode("secret"))
            .scopes("read")
            .authorizedGrantTypes("authorization_code")
            .redirectUris("http://localhost:3000/oauth/redirect")
    }
}