package nl.atsp.oauth2server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer

@SpringBootApplication
@EnableAuthorizationServer
class OAuth2Server

fun main(args: Array<String>)
{
    runApplication<OAuth2Server>(*args)
}
