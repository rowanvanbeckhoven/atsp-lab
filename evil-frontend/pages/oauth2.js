async function startAuth() {
        const clientId = 'atsp-frontend';
        const redirectUri = 'http://localhost:3000/oauth/redirect';
        const authUrl = 'http://localhost:8200/oauth/authorize';
        const state = generateState('http://localhost:3001/evil/redirect');
        const scope = 'read';
        const type = 'code'
        const params = {
            client_id: clientId,
            redirect_uri: redirectUri,
            state: state,
            scope: scope,
            response_type: type
        }
        window.location.href = createRequest(authUrl, params)
}

function createRequest(baseUrl, params) {
   return baseUrl + '?' + new URLSearchParams(params).toString();
}

function generateState(redirectUrl) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < 32; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result + ';redirectUri=' + redirectUrl;
}